package com.samsaydali.activity;

import com.samsaydali.activity.services.EmployeeService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication()
public class ActivityApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActivityApplication.class, args);
    }

    @Bean
    public CommandLineRunner init(final EmployeeService employeeService) {

        return new CommandLineRunner() {
            public void run(String... strings) throws Exception {
                employeeService.createEmployee();
            }
        };
    }
}
