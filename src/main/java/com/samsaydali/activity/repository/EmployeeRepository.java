package com.samsaydali.activity.repository;


import com.samsaydali.activity.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    public Employee findByName(String name);

}